FROM elasticsearch:8.2.2
# Disable SSL
RUN sed -i 's/xpack.security.enabled: true/xpack.security.enabled: false/g' /usr/share/elasticsearch/config/elasticsearch.yml
ENTRYPOINT ["/bin/tini","--","/usr/local/bin/docker-entrypoint.sh"]