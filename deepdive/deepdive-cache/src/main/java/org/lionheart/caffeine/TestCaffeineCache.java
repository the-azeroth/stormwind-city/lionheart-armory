package org.lionheart.caffeine;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
public class TestCaffeineCache<T> {

    private final Cache<String, T> DEFAULT_CACHE = Caffeine.newBuilder()
            .expireAfterAccess(1, TimeUnit.HOURS)
            .maximumSize(365 * 5)
            .build();

    public TestCaffeineCache() {}

    public T getCache(String key) {
        return DEFAULT_CACHE.get(key, k -> getFromOrigin(Collections.singleton(k)).get(key));
    }

    public Map<String, T> getAllCache(Collection<String> keys) {
        return DEFAULT_CACHE.getAll(keys, ks -> getFromOrigin(keys));
    }

    public Map<String, T> getFromOrigin(Collection<String> keys) {
        return null;
    }
}
