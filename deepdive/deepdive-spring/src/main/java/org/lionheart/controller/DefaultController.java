package org.lionheart.controller;

import org.lionheart.service.ITestSpringTransactionalService;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

    private final ITestSpringTransactionalService iTestSpringTransactionalService;

    public DefaultController(ITestSpringTransactionalService iTestSpringTransactionalService) {
        this.iTestSpringTransactionalService = iTestSpringTransactionalService;
    }


    @PutMapping("/test/test-spring-transactional")
    public Long testSpringTransactional() {
        iTestSpringTransactionalService.testSpringTransactional();
        return 0L;
    }
}
