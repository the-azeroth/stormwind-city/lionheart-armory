package org.lionheart.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.lionheart.model.SysUserModel;

@Mapper
public interface SysUserMapper {

    boolean insert(SysUserModel sysUserModel);
}
