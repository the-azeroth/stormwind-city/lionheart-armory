package org.lionheart.service;

public interface ISimpAttributeService {

    String getAttribute() throws Exception;
}
