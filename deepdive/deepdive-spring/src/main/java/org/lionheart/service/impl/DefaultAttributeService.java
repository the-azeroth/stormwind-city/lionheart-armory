package org.lionheart.service.impl;

import org.lionheart.service.ISimpAttributeService;
import org.springframework.stereotype.Service;

@Service
public class DefaultAttributeService implements ISimpAttributeService {
    @Override
    public String getAttribute() throws Exception {
        return "Default Attributes";
    }
}
