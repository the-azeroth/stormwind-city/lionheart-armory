package org.lionheart.service.impl;

import org.lionheart.model.SysUserModel;
import org.lionheart.repository.ITestSpringTransactionalRepository;
import org.lionheart.service.ITestSpringTransactionalService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TestSpringTransactional implements ITestSpringTransactionalService {

    private final ITestSpringTransactionalRepository iTestSpringTransactionalRepository;

    public TestSpringTransactional(ITestSpringTransactionalRepository iTestSpringTransactionalRepository) {
        this.iTestSpringTransactionalRepository = iTestSpringTransactionalRepository;
    }

    @Transactional(rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
    @Override
    public void testSpringTransactional() {

        SysUserModel user = SysUserModel
                .builder()
                .id(10000L)
                .userId(10001L)
                .userName("TestSpringTransactional")
                .build();

        iTestSpringTransactionalRepository.createUser(user);

        iTestSpringTransactionalRepository.createUser(user);
    }
}
