package org.lionheart.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.lionheart.service.ISimpAttributeService;
import org.lionheart.service.IUserAttributeService;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserAttributeService implements IUserAttributeService {

    private final ISimpAttributeService iSimpAttributeService;

    public UserAttributeService(ISimpAttributeService iSimpAttributeService) {
        this.iSimpAttributeService = iSimpAttributeService;
    }

    @Override
    public String getUserAttribute() {
        String userAttributes = null;
        try {
            userAttributes = iSimpAttributeService.getAttribute();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        log.info(userAttributes);
        return userAttributes;
    }
}
