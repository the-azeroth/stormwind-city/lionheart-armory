package org.lionheart.config;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScans({
        @MapperScan(basePackages = {"org.lionheart.mapper"})
})
public class MybatisConfig {

}
