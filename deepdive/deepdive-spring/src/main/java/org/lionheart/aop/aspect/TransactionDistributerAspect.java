package org.lionheart.aop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TransactionDistributerAspect {
    @Before(value = "@annotation(org.lionheart.aop.annotation.TransactionDistributer)")
    public void getTransactionDistributer(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    }
}
