package org.lionheart.repository;

import org.lionheart.model.SysUserModel;

public interface ITestSpringTransactionalRepository {

    boolean createUser(SysUserModel sysUserModel);
}
