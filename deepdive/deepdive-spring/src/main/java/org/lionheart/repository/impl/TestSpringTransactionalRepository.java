package org.lionheart.repository.impl;

import org.lionheart.mapper.SysUserMapper;
import org.lionheart.model.SysUserModel;
import org.lionheart.repository.ITestSpringTransactionalRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class TestSpringTransactionalRepository implements ITestSpringTransactionalRepository {

    private final SysUserMapper sysUserMapper;

    public TestSpringTransactionalRepository(SysUserMapper sysUserMapper) {
        this.sysUserMapper = sysUserMapper;
    }

    @Transactional(rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
    @Override
    public boolean createUser(SysUserModel sysUserModel) {
        return sysUserMapper.insert(sysUserModel);
    }
}
