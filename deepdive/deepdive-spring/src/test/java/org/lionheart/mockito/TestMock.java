package org.lionheart.mockito;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.lionheart.service.ISimpAttributeService;
import org.lionheart.service.impl.UserAttributeService;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class TestMock {

//    @Mock
//    ISimpAttributeService iSimpAttributeService;

    @Spy
    ISimpAttributeService iSimpAttributeService;

    @InjectMocks
    UserAttributeService userAttributeService;

    @Test
    public void voidTestMock() throws Exception {
        Mockito.when(iSimpAttributeService.getAttribute()).thenReturn("Override Attributes");
        Mockito.doReturn("Override Attributes").when(iSimpAttributeService).getAttribute();
        Assertions.assertEquals("Override Attributes", userAttributeService.getUserAttribute());
    }
}
