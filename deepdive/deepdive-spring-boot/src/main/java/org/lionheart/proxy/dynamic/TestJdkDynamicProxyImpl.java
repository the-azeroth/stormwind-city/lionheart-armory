package org.lionheart.proxy.dynamic;

public class TestJdkDynamicProxyImpl implements TestJdkDynamicProxy{

    @Override
    public void test() {
        System.out.println("test");
    }

    public static void main(String[] args) {
        TestJdkDynamicProxy proxy = (TestJdkDynamicProxy) ProxyFactory.getProxy(new TestJdkDynamicProxyImpl());
        proxy.test();
    }
}
