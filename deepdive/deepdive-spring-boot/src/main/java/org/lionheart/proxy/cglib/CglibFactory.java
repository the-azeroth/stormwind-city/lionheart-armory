package org.lionheart.proxy.cglib;

import org.springframework.cglib.proxy.Enhancer;

import java.io.File;
import java.io.FileOutputStream;

public class CglibFactory {
    public static Object getProxy(Class<?> clazz) {
        Enhancer enhancer = new Enhancer();
        enhancer.setClassLoader(clazz.getClassLoader());
        enhancer.setSuperclass(clazz);
        enhancer.setCallback(new TargetInterceptor());
        Object obj = enhancer.create();
        extracted(enhancer);
        return obj;
    }

    private static void extracted(Enhancer enhancer) {
        try {
            byte[] generate = new byte[0];
            generate = enhancer.getStrategy ().generate (enhancer);
            FileOutputStream fileOutputStream = new FileOutputStream (
                    new File("./CglibProxy.class"));
            fileOutputStream.write (generate);
            fileOutputStream.flush ();
            fileOutputStream.close ();
        } catch (Exception e) {
            e.printStackTrace ();
        }
    }
}
