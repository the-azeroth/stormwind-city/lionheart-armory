package org.lionheart.proxy.dynamic;

import java.lang.reflect.Proxy;

public class ProxyFactory {
    public static Object getProxy(Object obj) {
        Class<?> clazz = obj.getClass();
        return Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(), new TargetInvoker(obj));
    }
}
