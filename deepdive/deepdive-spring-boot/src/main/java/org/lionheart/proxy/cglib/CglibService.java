package org.lionheart.proxy.cglib;

public class CglibService {

    public void test() {
        System.out.println("test");
    }

    public static void main(String[] args) {
        CglibService proxy = (CglibService) CglibFactory.getProxy(CglibService.class);
        proxy.test();
    }
}
