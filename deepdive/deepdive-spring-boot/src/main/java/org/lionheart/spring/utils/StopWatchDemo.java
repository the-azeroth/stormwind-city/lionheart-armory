package org.lionheart.spring.utils;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;

@Slf4j
public class StopWatchDemo {

    @SneakyThrows
    public static void main(String[] args) {
        StopWatch watch = new StopWatch();
        watch.start("Raise StopWatch");

        log.info("StopWatch: {}", watch);
    }
}
