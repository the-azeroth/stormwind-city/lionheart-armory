package org.lionheart.lombok;

import lombok.SneakyThrows;

import java.io.FileInputStream;

public class SneakyThrowsDemo {

    @SneakyThrows()
    public void test() {
        new FileInputStream("index.html");
    }

    public static void main(String[] args) {
        SneakyThrowsDemo demo = new SneakyThrowsDemo();
        demo.test();
    }
}
