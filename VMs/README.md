Vagrant box
https://app.vagrantup.com/debian/boxes/bullseye64/versions/11.20220328.1/providers/virtualbox.box

Vagrant Init
```shell
# vagrant init
```

```shell
# vagrant up
```

```shell
# vagrant ssh-config
```

```shell
# vagrant ssh vagrant@192.168.33.10 -i ${IdentityFile}
```

Fix the vim direction key issue 
```shell
# sed -i -E 's/(deb|security).debian.org/mirrors.aliyun.com/g' /etc/apt/sources.list
# sudo apt-get remove vim-common
# sudo apt-get install vim
```

# TODO 配置 kubelet cgroup

# TODO 准备集群基础镜像 
sudo kubeadm config images pull

vagrant@bullseye:~$ kubeadm config images list
k8s.gcr.io/kube-apiserver:v1.23.5
k8s.gcr.io/kube-controller-manager:v1.23.5
k8s.gcr.io/kube-scheduler:v1.23.5
k8s.gcr.io/kube-proxy:v1.23.5
k8s.gcr.io/pause:3.6
k8s.gcr.io/etcd:3.5.1-0
k8s.gcr.io/coredns/coredns:v1.8.6

kubeadm 国内镜像源
https://zhuanlan.zhihu.com/p/55740564

# TODO 集群初始化 
sudo kubeadm init \
--kubernetes-version=v1.23.5 \
--pod-network-cidr=10.244.0.0/16 \
--service-cidr=10.96.0.0/12 \
--apiserver-advertise-address=192.168.33.100

# TODO 注册node节点

# TODO 安装网络插件


