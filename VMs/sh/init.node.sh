# 配置时间同步客户端
sudo cp /etc/chrony/chrony.conf /etc/chrony/chrony.conf.bak
sudo cp $HOME/resources/chrony.client.conf /etc/chrony/chrony.conf
sudo timedatectl set-ntp yes
sudo systemctl restart chrony

# 开启客户端定时时间同步
echo "*/1 * * * * root /usr/sbin/ntpdate master" | sudo tee -a /etc/crontab