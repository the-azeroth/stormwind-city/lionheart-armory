# 配置时间同步服务器
sudo cp /etc/chrony/chrony.conf /etc/chrony/chrony.conf.bak
sudo cp $HOME/resources/chrony.service.conf /etc/chrony/chrony.conf
sudo timedatectl set-ntp yes
sudo systemctl restart chrony
