package lionheart.armory.controller;

import lionheart.armory.common.constants.CommonConstants;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController()
@RequestMapping("/role")
public class RoleController {

    @GetMapping("/{role}")
    public Long getRole(@PathVariable("role") CommonConstants.SystemUserRole role) {
        return role.getInternalValue();
    }

    @PostMapping()
    public List<Long> associateUserRoles(@RequestBody() List<CommonConstants.SystemUserRole> roles) {
        return roles.stream().map(CommonConstants.SystemUserRole::getInternalValue).collect(Collectors.toList());
    }
}
