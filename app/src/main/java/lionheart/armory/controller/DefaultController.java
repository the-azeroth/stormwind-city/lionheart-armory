package lionheart.armory.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

@RestController
public class DefaultController {

    private static final Logger logger = LoggerFactory.getLogger(DefaultController.class);

    private ObjectMapper objectMapper;

    public DefaultController(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @GetMapping(value = "ping")
    public Set<String> health() {
        Set<String> set = new HashSet<>();
        set.add("pong!");
        try {
            String health = objectMapper.writeValueAsString(set);
            logger.info("health check: {}.", health);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return set;
    }
}
