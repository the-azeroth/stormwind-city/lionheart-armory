package lionheart.armory.common.config;

import lionheart.armory.common.constants.ConverterBaseEnum;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class DefaultWebMvcConfigurer implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new ConverterFactory<String, ConverterBaseEnum>() {
            @Override
            public <T extends ConverterBaseEnum> Converter<String, T> getConverter(Class<T> targetType) {
                return source -> Arrays.stream(targetType.getEnumConstants())
                        .filter(e -> e.getExternalValue().equals(source))
                        .findFirst()
                        .orElseThrow();
            }
        });
    }
}
