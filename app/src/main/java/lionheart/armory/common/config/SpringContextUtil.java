package lionheart.armory.common.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class SpringContextUtil implements ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(SpringContextUtil.class);

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(@NonNull ApplicationContext context) throws BeansException {
        logger.info("ApplicationContextAware Initialized");
        applicationContext = context;
    }

    public static <T> T getBean(Class<T> clazz) throws Exception {
        if(applicationContext==null) {
            throw new Exception("ApplicationContext is null");
        }
        return applicationContext.getBean(clazz);
    }

    public static Object getBean(String name) throws Exception {
        if(applicationContext==null) {
            throw new Exception("ApplicationContext is null");
        }
        return applicationContext.getBean(name);
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}