package lionheart.armory.common.constants;

import java.util.Arrays;

public class CommonConstants {

    public enum SystemUserRole implements ConverterBaseEnum<String> {
        SYSTEM_ADMIN(99999L, "SYSADMIN", "Default System Administrator Role"),
        NORMAL_USER(10000L, "USER", "Default Normal User Role");

        private final Long roleId;
        private final String roleName;
        private final String roleDesc;

        SystemUserRole(Long roleId, String roleName, String roleDesc) {
            this.roleId = roleId;
            this.roleName = roleName;
            this.roleDesc = roleDesc;
        }

        public Long getInternalValue() {
            return this.roleId;
        }

        @Override
        public String getExternalValue() {
            return this.roleName;
        }

        @Override
        public String toString() {
            return "SystemUserRole{" +
                    "roleId=" + roleId +
                    ", roleName='" + roleName + '\'' +
                    ", roleDesc='" + roleDesc + '\'' +
                    '}';
        }
    }
}
