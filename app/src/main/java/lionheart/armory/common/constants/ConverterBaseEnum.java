package lionheart.armory.common.constants;

public interface ConverterBaseEnum<T> {
    T getExternalValue();
}
