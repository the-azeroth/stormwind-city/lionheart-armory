package lionheart.armory.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lionheart.armory.common.constants.CommonConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.List;

@WebMvcTest(value = {RoleController.class})
public class RoleControllerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoleControllerTest.class);

    private final MockMvc mockMvc;

    private final ObjectMapper objectMapper;

    @Autowired
    public RoleControllerTest(MockMvc mockMvc, ObjectMapper objectMapper) {
        this.mockMvc = mockMvc;
        this.objectMapper = objectMapper;
    }

    @Test
    public void testStringToEnumConverter() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/role/SYSADMIN");
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        LOGGER.info(result.getResponse().getContentAsString());
    }

    @Test
    public void testPostMethod() throws Exception {
        List<String> list = Collections.singletonList(CommonConstants.SystemUserRole.NORMAL_USER.name());
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/role")
                .content(objectMapper.writeValueAsString(list))
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        List<Integer> convert = objectMapper.readValue(result.getResponse().getContentAsString(), List.class);
        Assertions.assertEquals(10000, convert.get(0));
    }
}
