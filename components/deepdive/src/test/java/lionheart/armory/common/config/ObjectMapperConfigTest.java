package lionheart.armory.common.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lionheart.armory.common.config.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@SpringBootTest(classes = {ObjectMapperConfig.class})
public class ObjectMapperConfigTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectMapperConfigTest.class);

    private static ClassPathResource USER;
    private static ClassPathResource USERS;

    public final ObjectMapper defaultObjectMapper;

    @Autowired
    public ObjectMapperConfigTest(
            @Qualifier("defaultObjectMapper") ObjectMapper defaultObjectMapper
    ) {
        LOGGER.info("defaultObjectMapper {}", defaultObjectMapper.toString());
        this.defaultObjectMapper = defaultObjectMapper;
    }

    @BeforeAll
    public static void beforeAll() {
        USER = new ClassPathResource("user.json");
        USERS = new ClassPathResource("users.json");
    }

    @Test
    public void testDefaultObjectMapper() throws IOException {
        User user = this.defaultObjectMapper.readValue(USER.getInputStream(), User.class);
        String userStr = this.defaultObjectMapper.writeValueAsString(user);
        LOGGER.info(userStr);
    }

    @Test
    public void testObjectMapperToArray() throws IOException {
        List<User> users = this.defaultObjectMapper.readValue(USERS.getInputStream(), new TypeReference<List<User>>() {});
        Assert.hasLength(String.valueOf(users.size()), "1");
    }

    @Test
    public void testObjectMapperToMap() throws IOException {
        Map<String, Object> user = defaultObjectMapper.readValue(USER.getInputStream(), new TypeReference<Map<String, Object>>(){});
        Assert.notEmpty(user, "empty");
    }
}
