package lionheart.armory.deepdive.freemarker;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import lionheart.armory.deepdive.freemarker.templateloader.AmazonS3TemplateLoader;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@SpringBootTest(classes = {FreeMarkerConfig.class})
@Testcontainers
public class TestFreeMarkerService {

    final static DockerImageName docker = DockerImageName.parse("localstack/localstack:0.14.2");

    static final String BUCKET_NAME = "foo-bucket";

    @Rule
    public static LocalStackContainer localStackContainer = new LocalStackContainer(docker).withServices(LocalStackContainer.Service.S3);

    @Rule
    public static Configuration cfg = new Configuration(Configuration.VERSION_2_3_29);

    public static AmazonS3 S3;

    @BeforeAll
    public static void beforeAll() throws IOException {
        localStackContainer.start();
        S3 = AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(localStackContainer.getEndpointConfiguration(LocalStackContainer.Service.S3))
                .withCredentials(localStackContainer.getDefaultCredentialsProvider())
                .build();
        S3.createBucket(BUCKET_NAME);
        S3.putObject(BUCKET_NAME, "Template.ftlh", new ClassPathResource("Template.ftlh").getFile());
        cfg.setTemplateLoader(new AmazonS3TemplateLoader(S3, BUCKET_NAME));
        cfg.setDefaultEncoding(StandardCharsets.UTF_8.name());
        cfg.setLocalizedLookup(false);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    }

    @Test
    public void testFindTemplateResource() {
        ObjectListing list = S3.listObjects(BUCKET_NAME);
        list.getObjectSummaries().forEach(System.out::println);

        Template template = null;
        try {
            template = cfg.getTemplate("Template.ftlh");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testRebuildResource() throws Exception {
        // TODO
    }
}
