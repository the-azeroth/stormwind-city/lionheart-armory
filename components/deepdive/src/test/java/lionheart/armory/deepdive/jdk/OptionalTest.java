package lionheart.armory.deepdive.jdk;

import lombok.Data;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class OptionalTest {

    @Data
    protected class TestModel {
        private List<SubTestModel> subTestModelList;
    }

    @Data
    protected class SubTestModel {
        private Long id;
    }

    @Test
    public void testOptional() {
        TestModel testModel = null;

        List<SubTestModel> list = Optional.ofNullable(testModel)
                .map(TestModel::getSubTestModelList)
                .orElse(Collections.emptyList());
        Assertions.assertNotNull(list);
    }
}
