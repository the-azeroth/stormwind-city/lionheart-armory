package lionheart.armory.deepdive.spring;

import lionheart.armory.Application;
import lionheart.armory.deepdive.frwk.spring.NonNullAnnotation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {Application.class, NonNullAnnotation.class})
public class TestNonNullAnnotation {

    private final NonNullAnnotation nonNullAnnotation;

    @Autowired
    public TestNonNullAnnotation(NonNullAnnotation nonNullAnnotation) {
        this.nonNullAnnotation = nonNullAnnotation;
    }

    @Test
    public void testNonNullAnnotation() {
        String nullParam = null;
        Assertions.assertThrows(IllegalArgumentException.class, () -> nonNullAnnotation.testNonNullAnnotation(nullParam));
    }
}
