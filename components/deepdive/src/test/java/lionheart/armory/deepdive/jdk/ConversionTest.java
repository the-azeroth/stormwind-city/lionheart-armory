package lionheart.armory.deepdive.jdk;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.DefaultConversionService;

public class ConversionTest {

    @Data
    @AllArgsConstructor
    protected class Point {
        private int x;
        private int y;
    }

    @Test
    public void testConvertStringToInteger() {

        final class PointerConverter implements Converter<String, Point> {
            @Override
            public Point convert(String source) {
                String[] splits = source.split(":");
                return new Point(Integer.parseInt(splits[0]), Integer.parseInt(splits[1]));
            }
        }

        DefaultConversionService conversion = new DefaultConversionService();
        conversion.addConverter(new PointerConverter());

        Point result = conversion.convert("345:234", Point.class);
        Assertions.assertEquals(345, result.getX());
        Assertions.assertEquals(234, result.getY());
    }
}
