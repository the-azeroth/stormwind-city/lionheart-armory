package lionheart.armory.deepdive.multithread;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestThread {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestThread.class);

    @Test
    public void testTread() {
        LOGGER.info("Main Start");
        Thread t = new Thread(() -> {
            LOGGER.info("Thread Start");
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            LOGGER.info("Thread End");
        });
        t.start();
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        LOGGER.info("Main End");
    }

    @Test
    public void testThread() throws InterruptedException {
        LOGGER.info("Main Start");
        Thread t = new Thread(() -> {
            LOGGER.info("Thread Start");
            LOGGER.info("Thread End");
        });
        t.start();
        t.join();
        LOGGER.info("Main End");
    }
}
