package lionheart.armory.deepdive.guava;

import com.google.common.base.Optional;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.testcontainers.shaded.com.google.common.collect.BiMap;
import org.testcontainers.shaded.com.google.common.collect.HashBiMap;

/**
 * This class is for the Guava style collections article.
 */
public class TestGuavaCollections {

    @Test
    public void testGuavaOptional() {
        // Guava Optional vs JDK Optional
        Assertions.assertThrows(NullPointerException.class, () -> java.util.Optional.of(null));
        Assertions.assertThrows(NullPointerException.class, () -> Optional.of(null));

        java.util.Optional.ofNullable(null); // return new Optional(null);
        Optional.fromNullable(null); // return Optional.absent();

        Optional op1 = Optional.absent();

    }

    @Test
    public void testBiMap() {
        BiMap<Long, Long> biMap = HashBiMap.create();
        biMap.put(1L, 2L);
        biMap.put(2L, 3L);
        biMap.put(3L, 4L);

        Assertions.assertEquals(2L, biMap.inverse().get(3L));
        Assertions.assertThrows(IllegalArgumentException.class, () -> biMap.put(1L, 3L));
        biMap.forcePut(1L, 3L);
        Assertions.assertEquals(1L, biMap.inverse().get(3L));
    }

    @Test
    public void testTable() {
        Table<Long, Long, Long> coordinate = HashBasedTable.create();
        coordinate.put(1L, 1L, 1L);
        coordinate.put(1L, 1L, 2L);

        Assertions.assertEquals(2L, coordinate.get(1L, 1L));
    }

    @Test
    public void testTable2() {
        /**
         * Whs - item - div - whs - lc/pq
         */
//        Table<Long, String, Long>
    }
}
