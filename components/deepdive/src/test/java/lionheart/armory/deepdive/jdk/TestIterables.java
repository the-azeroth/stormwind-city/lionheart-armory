package lionheart.armory.deepdive.jdk;

import com.google.common.collect.Iterables;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class TestIterables {

    @Test
    public void testIterablesGetOnlyElement() {
        List<String> list = null;
        List<String> optList = Optional.ofNullable(list).orElse(Collections.emptyList());
        String onlyVal = Iterables.getOnlyElement(optList, "");
    }
}
