package lionheart.armory.deepdive.mybatis;

import lionheart.armory.deepdive.mybatis.mapper.SysUserMapper;
import lionheart.armory.deepdive.mybatis.model.SysUserModel;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;

@SpringBootTest
@Slf4j
public class TestMybatis {

    private final DataSource dataSource;

    private final SysUserMapper sysUserMapper;

    @Autowired
    public TestMybatis(DataSource dataSource, SysUserMapper sysUserMapper) {
        this.dataSource = dataSource;
        this.sysUserMapper = sysUserMapper;
    }

    @Test
    public void testInsert() {
        SysUserModel sysUserModel = new SysUserModel();
        sysUserModel.setUserId(10000L);
        sysUserModel.setUserName("Admin");
        sysUserMapper.insert(sysUserModel);
//        log.info(sysUserModel.getId() + "," + sysUserModel.getUserId() + "," + sysUserModel.getUserName());
        log.info(sysUserModel.getUserId() + "," + sysUserModel.getUserName());
    }
}
