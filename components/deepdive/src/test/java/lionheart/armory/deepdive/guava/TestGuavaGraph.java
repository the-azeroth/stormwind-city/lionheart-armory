package lionheart.armory.deepdive.guava;

import lombok.Data;

public class TestGuavaGraph {

    @Data
    protected class Item {
        private Long itemId;
        private Long itemName;
    }

    @Data
    protected class Division {
        private Long divisionId;
        private Long divisionName;
    }

    @Data
    protected class Warehouse {
        private Long warehouseId;
        private Long warehouseName;
    }

    @Data
    protected class Store {
        private Long storeId;
        private Long storeName;
    }
}
