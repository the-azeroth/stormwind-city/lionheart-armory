package lionheart.armory.deepdive.caffeine;


import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

@Slf4j
public class TestCaffeine {

    private static final Cache<String, Object> cache = Caffeine.newBuilder()
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .maximumSize(10_000)
            .build();

    @Test
    public void test() {
        String key = "key";
        Object obj = cache.get(key, o -> {
            log.info(o);
            return cacheDataProvider(o);
        });
    }

    private Object cacheDataProvider(String key) {
        return null;
    }
}
