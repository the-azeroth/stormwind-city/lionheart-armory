package lionheart.armory.deepdive.jdk;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class TestStream {

    /*
    Stream will not be executed without invoking the final operation.
    Stream could be consumed only once.
     */

    @Test
    public void testStreamCollect() {

        ArrayList<Long> list = new ArrayList<>(10);
        // Save Stream result to Collection
        // To List
        List<Long> arr = list.stream().filter(e -> e > 0).collect(Collectors.toList());
        // To Set
        Set<Long> set = list.stream().filter(e -> e > 0).collect(Collectors.toSet());
        // Save Stream result to a specific Collection
        // To ArrayList
        ArrayList<Long> arr2 = list.stream().filter(e -> e > 0).collect(Collectors.toCollection(ArrayList::new));
        // To HasSet
        HashSet<Long> set1 = list.stream().filter(e -> e > 0).collect(Collectors.toCollection(HashSet::new));

        // Save Stream result to Array
        // Non-args
        Object[] objArr = list.stream().filter(e -> e > 0).toArray();
        Long[] longArr = list.stream().filter(e -> e > 0).toArray(Long[]::new);

        // Operations
        ArrayList<Student> studentList = new ArrayList<>(3);
        studentList.add(new Student(1L, "a", 24, 88L));
        studentList.add(new Student(2L, "b", 22, 67L));
        studentList.add(new Student(3L, "c", 26, 73L));

        studentList.stream().filter(e -> e.getId() > 0).max((a, b) -> (int) (a.getScore() - b.getScore()));
        studentList.stream().filter(e -> e.getId() > 0).collect(Collectors.maxBy((a, b) -> (int) (a.getScore() - b.getScore())));

        studentList.stream().filter(e -> e.getId() > 0).min((a, b) -> (int) (a.getScore() - b.getScore()));
        studentList.stream().filter(e -> e.getId() > 0).collect(Collectors.minBy((a, b) -> (int) (a.getScore() - b.getScore())));

        studentList.stream().filter(e -> e.getId() > 0).map(Student::getScore).reduce(0L, Long::sum);
        studentList.stream().filter(e -> e.getId() > 0).collect(Collectors.summarizingLong(Student::getScore));
        studentList.stream().filter(e -> e.getId() > 0).collect(Collectors.averagingLong(Student::getScore));

        studentList.stream().filter(e -> e.getId() > 0).count();
        studentList.stream().filter(e -> e.getId() > 0).collect(Collectors.counting());

        // Group
        Map<Integer, Optional<Student>> map = studentList.stream().filter(e -> e.getId() > 0)
                .collect(
                        Collectors.groupingBy(
                                Student::getAge,
                                Collectors.reducing(
                                        BinaryOperator.maxBy(
                                                Comparator.comparingLong(Student::getScore)
                                        )
                                )
                        )
                );
        map.forEach((key, value) -> {
            System.out.println(key + "-----" + value.get());
        });
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    class Student {
        private Long id;
        private String name;
        private Integer age;
        private Long score;
    }
}
