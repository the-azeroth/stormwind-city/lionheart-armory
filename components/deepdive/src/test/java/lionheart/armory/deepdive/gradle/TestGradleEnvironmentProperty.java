package lionheart.armory.deepdive.gradle;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestGradleEnvironmentProperty {

    @Test
    public void testPropertyFromGradle() {
        Assertions.assertEquals("BAR", System.getProperty("FOO"));
    }

    @Test
    public void testEnvironmentVariableFromGradle() {
        Assertions.assertEquals("BAZ", System.getenv("FOO"));
    }
}
