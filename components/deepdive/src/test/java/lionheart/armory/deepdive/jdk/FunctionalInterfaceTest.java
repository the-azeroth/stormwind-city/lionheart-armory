package lionheart.armory.deepdive.jdk;

import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.BiPredicate;
import java.util.function.Function;

public class FunctionalInterfaceTest {

    public static final Logger LOGGER = LoggerFactory.getLogger(FunctionalInterfaceTest.class);

    @FunctionalInterface
    protected interface FunInterface<T> {
        T execute();
    }

    Function<String, String> fun = message -> message;

    public <T> T buildUserRole(String role, FunInterface<T> fun) {
        // build
        return fun.execute();
    }

    @Test
    public void testLambda() {
        this.buildUserRole("role", () -> null);
    }

    @Test
    public void testBiPredicate() {
        ImmutableTable<Long, Long, Long> table = ImmutableTable.<Long, Long, Long>builder()
                .put(1L,2L,3L)
                .put(1L,3L,4L)
                .put(1L,4L,5L)
                .put(1L,5L,6L)
                .build();
        Assertions.assertTrue(checkExist.test(table, 1L));
    }

    private final BiPredicate<Table<Long, Long, Long>, Long> checkExist = Table::containsRow;
}
