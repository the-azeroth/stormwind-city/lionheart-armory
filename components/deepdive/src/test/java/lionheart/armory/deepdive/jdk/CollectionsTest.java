package lionheart.armory.deepdive.jdk;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CollectionsTest {

    @Test
    public void testCollection() {
        List<Long> l1 = new ArrayList<>(4);
        List<Long> l2 = new ArrayList<>(6);

        l1.add(1L);
        l1.add(2L);
        l1.add(3L);
        l1.add(4L);

        l2.add(2L);
        l2.add(3L);
        l2.add(4L);
        l2.add(5L);
        l2.add(6L);
        l2.add(7L);

        // And Set
        l1.retainAll(l2);

        Assertions.assertEquals(3, l1.size());

        List<Long> l3 = new ArrayList<>(4);
        List<Long> l4 = new ArrayList<>(6);

        l3.add(1L);
        l3.add(2L);
        l3.add(3L);
        l3.add(4L);

        l4.add(2L);
        l4.add(3L);
        l4.add(4L);
        l4.add(5L);
        l4.add(6L);
        l4.add(7L);

        l3.removeAll(l4);
        l3.addAll(l4);

        Assertions.assertEquals(6, l3.size());

        List<Long> l5 = new ArrayList<>(4);
        List<Long> l6 = new ArrayList<>(6);

        l5.add(1L);
        l5.add(2L);
        l5.add(3L);
        l5.add(4L);

        l6.add(2L);
        l6.add(3L);
        l6.add(4L);
        l6.add(5L);
        l6.add(6L);
        l6.add(7L);

        Set<Long> interList = new HashSet(CollectionUtils.intersection(l5, l6));
    }
}
