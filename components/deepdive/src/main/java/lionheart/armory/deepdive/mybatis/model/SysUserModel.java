package lionheart.armory.deepdive.mybatis.model;


import lombok.Data;

@Data
public class SysUserModel {

//    private Long id;

    private Long userId;

    private String userName;
}
