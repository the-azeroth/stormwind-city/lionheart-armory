package lionheart.armory.deepdive.freemarker.templateloader;

import freemarker.cache.TemplateLoader;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class ClassPathResourceTemplateLoader implements TemplateLoader {

    private static final String TEMPLATE_PATH = "/templates/";

    @Override
    public Object findTemplateSource(String name) throws IOException {
        ClassPathResource cpr = new ClassPathResource(TEMPLATE_PATH + name);
        return cpr.getInputStream();
    }

    @Override
    public long getLastModified(Object templateSource) {
        return -1;
    }

    @Override
    public Reader getReader(Object templateSource, String encoding) throws IOException {
        InputStream cpr = (InputStream) templateSource;
        return new InputStreamReader(cpr);
    }

    @Override
    public void closeTemplateSource(Object templateSource) throws IOException {
        InputStream cpr = (InputStream) templateSource;
        cpr.close();
    }
}
