package lionheart.armory.deepdive.frwk.spring;

import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class TestSpringDI {

    private final Map<String, TestService> nameToBeanMap;

    public TestSpringDI(Map<String, TestService> nameToBeanMap) {
        this.nameToBeanMap = nameToBeanMap;
    }
}

interface TestService {
    void foo();
}

@Service
class TestServiceImplA implements TestService {
    public void foo() {}
}

@Service
class TestServiceImplB implements TestService {
    public void foo() {}
}