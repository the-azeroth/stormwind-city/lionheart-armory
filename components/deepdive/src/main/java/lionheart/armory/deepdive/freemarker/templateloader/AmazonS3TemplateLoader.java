package lionheart.armory.deepdive.freemarker.templateloader;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import freemarker.cache.TemplateLoader;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class AmazonS3TemplateLoader implements TemplateLoader {

    private final AmazonS3 S3;

    private final String BUCKET_NAME;

    public AmazonS3TemplateLoader(AmazonS3 S3, String BUCKET_NAME) {
        this.S3 = S3;
        this.BUCKET_NAME = BUCKET_NAME;
    }

    @Override
    public Object findTemplateSource(String name) throws IOException {
        S3Object obj = S3.getObject(BUCKET_NAME, name);
        return obj;
    }

    @Override
    public long getLastModified(Object templateSource) {
        S3Object obj = (S3Object) templateSource;
        System.out.println(obj);
        return -1;
    }

    @Override
    public Reader getReader(Object templateSource, String encoding) throws IOException {
        S3Object obj = (S3Object) templateSource;
        return new InputStreamReader(obj.getObjectContent(), encoding);
    }

    @Override
    public void closeTemplateSource(Object templateSource) throws IOException {
        // Do nothing
    }
}
