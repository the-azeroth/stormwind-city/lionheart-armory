package lionheart.armory.deepdive.frwk.spring;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class NonNullAnnotation {

    public void testNonNullAnnotation(@NonNull String str) throws IllegalArgumentException {
        if (StringUtils.isEmpty(str)) throw new IllegalArgumentException("The argument is invalid.");
        log.info(str);
    }
}
