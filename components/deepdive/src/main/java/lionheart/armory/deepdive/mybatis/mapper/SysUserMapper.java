package lionheart.armory.deepdive.mybatis.mapper;

import lionheart.armory.deepdive.mybatis.model.SysUserModel;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserMapper {

    void insert(SysUserModel sysUserModel);
}
