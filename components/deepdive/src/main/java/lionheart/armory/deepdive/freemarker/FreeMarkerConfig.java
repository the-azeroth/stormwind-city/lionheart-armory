package lionheart.armory.deepdive.freemarker;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import lionheart.armory.deepdive.freemarker.templateloader.ClassPathResourceTemplateLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.io.*;
import java.nio.charset.StandardCharsets;

@Configuration
public class FreeMarkerConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(FreeMarkerConfig.class);

    static freemarker.template.Configuration cfg = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_29);

    static {
        cfg.setTemplateLoader(new ClassPathResourceTemplateLoader());
        cfg.setDefaultEncoding(StandardCharsets.UTF_8.name());
        cfg.setLocalizedLookup(false);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    }

    public static OutputStream generate(String templateName, Object data, OutputStream stream) throws IOException {
        Template template = cfg.getTemplate(templateName);
        Writer writer = new BufferedWriter(new OutputStreamWriter(new BufferedOutputStream(stream)));
        try {
            template.process(data, writer);
        } catch (TemplateException e) {
            LOGGER.error("generate HTML template error");
            throw new RuntimeException(e);
        }
        return stream;
    }
}
