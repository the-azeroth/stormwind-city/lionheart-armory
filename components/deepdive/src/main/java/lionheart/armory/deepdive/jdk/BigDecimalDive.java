package lionheart.armory.deepdive.jdk;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalDive {

    private final BigDecimal val1 = new BigDecimal(10L); // long
    private final BigDecimal val2 = new BigDecimal(10);  // int
    private final BigDecimal val3 = new BigDecimal(0.2); // double
    private final BigDecimal val4 = new BigDecimal("7.5"); // String

    private void testMethods() {
        val1.add(val2);
        val1.subtract(val2);
        val1.multiply(val2);
        val1.divide(val2, 2, RoundingMode.HALF_UP);
    }

    public static void main(String[] args) {
        BigDecimalDive bigDecimalDive = new BigDecimalDive();
        bigDecimalDive.testMethods();
    }
}
