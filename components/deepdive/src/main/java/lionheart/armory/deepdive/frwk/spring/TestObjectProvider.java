package lionheart.armory.deepdive.frwk.spring;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TestObjectProvider {

    private final TestBean testBean;

    public TestObjectProvider(ObjectProvider<TestBean> testBeanObjectProvider) {
        testBean = testBeanObjectProvider.getIfUnique();
    }

    @Getter
    public class TestBean implements TestBeanInterface {
        private final int instanceId;
        public TestBean(int i){
            instanceId = i;
        }
    }

    interface TestBeanInterface {

    }

    @Bean
    public TestBean testBean() {
        return new TestBean(1);
    }

    @Bean
    public TestBean testBean2() {
        return new TestBean(2);
    }
}