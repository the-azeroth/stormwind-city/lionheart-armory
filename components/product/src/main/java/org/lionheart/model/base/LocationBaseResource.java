package org.lionheart.model.base;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationBaseResource {

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private static final String SEPARATOR = " - ";

    private Long locationId;
    private String locationName;
    private String externalId;

    public String getDisplayName() {
        return this.externalId + SEPARATOR + this.locationName;
    }
}
