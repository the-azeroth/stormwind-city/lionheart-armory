package org.lionheart.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemResource {
    private Long itemId;
    private String itemName;
    private String priceGroup;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemResource that = (ItemResource) o;
        return Objects.equals(getItemId(), that.getItemId()) && Objects.equals(getItemName(), that.getItemName()) && Objects.equals(getPriceGroup(), that.getPriceGroup());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getItemId(), getItemName(), getPriceGroup());
    }
}
