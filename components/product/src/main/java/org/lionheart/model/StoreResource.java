package org.lionheart.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.lionheart.model.base.LocationBaseResource;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreResource extends LocationBaseResource {
    private String address;
    private String city;
    private String county;
    private String province;
    private String country;
    private String state;
}
