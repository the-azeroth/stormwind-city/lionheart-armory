package org.lionheart.model;

public abstract class ProductResource {

    private final static InheritableThreadLocal productResource = new InheritableThreadLocal();

    private ItemModel itemModel;
    private DivisionModel divisionModel;
    private WarehouseModel warehouseModel;
    private StoreModel storeModel;

    public static void productResource() {}
}
