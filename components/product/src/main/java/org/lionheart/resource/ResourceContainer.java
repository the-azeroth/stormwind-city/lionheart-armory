package org.lionheart.resource;

/**
 * Abstract Class for Resource
 */
public class ResourceContainer<T> implements AutoCloseable {

    private static final InheritableThreadLocal<Object> inheritableThreadLocal = new InheritableThreadLocal<>();

    void setResource(T t) {
        inheritableThreadLocal.set(t);
    }

    T getResource() {
        return (T) inheritableThreadLocal.get();
    }

    @Override
    public void close() throws Exception {
        inheritableThreadLocal.remove();
    }
}
