package org.lionheart.resource;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import org.lionheart.model.ItemResource;
import org.lionheart.model.base.LocationBaseResource;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ProductResourceContainer {

    private static final InheritableThreadLocal inheritableThreadLocal = new InheritableThreadLocal();

    private final Map<Long, ItemResource> itemResourceMap = new HashMap<>();
    private final Table<String, Long, LocationBaseResource> locationResourceMap = HashBasedTable.create();

    private final ArrayListMultimap<Long, Long> ocadoShedToVirtualStoreMap = ArrayListMultimap.create();
    /**
     * item to locations mapping table
     * Row - Delivery Type Warehouse/DSD
     * Column - itemId
     * Value -
     * -------------------------------------------------
     * |           | item 1 | item 2 | item 3 | item 4 |
     * -------------------------------------------------
     * | warehouse | obj 1  | obj 2  | obj 3  | obj 4  |
     * | DSD       | obj 1  | obj 2  | obj 3  | obj 4  |
     * -------------------------------------------------
     */

    private final Multimap<Long, Long> itemXdivIds = ArrayListMultimap.create();

}
